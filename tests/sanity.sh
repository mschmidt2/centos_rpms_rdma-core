#!/bin/bash
#
# This is a simple sanity test to satisfy the RHEL8.1 onboard gating
# requirement.

rdma-ndd --help
ret=$?

exit $ret
